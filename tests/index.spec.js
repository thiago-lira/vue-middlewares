import vueMiddleware from '../src/index';

let nextMock = null;

beforeEach(() => {
  nextMock = jest.fn();
});

describe('Vue Middlewares', () => {
  test('calls middlewares', () => {
    const middlewareMock = jest.fn(({ next }) => next());
    vueMiddleware({
      meta: {
        middlewares: () => [middlewareMock, middlewareMock],
      },
    },
    {},
    nextMock);

    expect(middlewareMock).toHaveBeenCalled();
    expect(nextMock).toHaveBeenCalled();
  });

  test('doesnt call middlewares', () => {
    vueMiddleware({}, {}, nextMock);
    expect(nextMock).toHaveBeenCalled();
  });

  test('calls next because middlewares list is empty', () => {
    vueMiddleware({ meta: { middlewares: () => [] } }, {}, nextMock);
    expect(nextMock).toHaveBeenCalled();
  });

  test('thorws exceptions when middleware is not a function', () => {
    const vueMiddlewareTest = () => {
      vueMiddleware({
        meta: {
          middlewares: () => ['notFunction'],
        },
      });
    };

    expect(vueMiddlewareTest).toThrow();
  });
});
