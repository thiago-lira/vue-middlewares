const isFunction = (fn) => {
  if (typeof fn !== 'function') throw new Error('You should provide only function.');

  return true;
};

const vueMiddlewares = (to, from, next) => ({
  middlewares: null,
  context: null,
  hasMiddlewares() {
    return to.meta && to.meta.middlewares;
  },
  getContext() {
    const context = { to, from };
    context.next = () => {
      if (this.middlewares.length) {
        const fn = this.middlewares.shift();
        if (isFunction(fn)) {
          fn(this.context);
        }
      } else {
        next();
      }
    };
    return context;
  },
  dispatchMiddleware() {
    if (Array.isArray(this.middlewares) && this.middlewares.length) {
      const firstMiddleware = this.middlewares.shift();

      this.context = this.getContext();
      if (isFunction(firstMiddleware)) {
        firstMiddleware(this.context);
      }
    } else {
      next();
    }
  },
  init() {
    if (this.hasMiddlewares()) {
      this.middlewares = to.meta.middlewares();
      this.dispatchMiddleware();
    } else {
      next();
    }
  },
});

export default (to, from, next) => {
  vueMiddlewares(to, from, next).init();
};
