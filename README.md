# Vue Middlewares

Just a simple package for Vue.js that allows to provide a list of middlewares.

## Usage

Import to your project:

```sh
yarn add @thiagolira/vue-middlewares
```

And then:
```javascript
import VueRouter from 'vue-router';
import vueMiddlewares from '@thiagolira/vue-middlewares';

import { myMiddleware1, myMiddleware2 } from 'path/to/middlewares';

import MyComponent from 'path/to/MyComponent.vue';

const router = new VueRouter({
  routes: [{
    path: '/login',
    name: 'login',
    component: MyComponent,
    meta: {
      middlewares: () => [myMiddleware1, myMiddleware2]
    }
  }]
})

router.beforeEach(vueMiddlewares)
```

Middlewares
```javascript
export default {
  myMiddleware1({ to, from, next }) {
    // do something
    next()
  },
  myMiddleware2({ to, from, next }) {
    // do something
    next()
  },
}

```

